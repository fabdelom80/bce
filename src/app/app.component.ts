import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: ` <router-outlet></router-outlet> `,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
      }

      .home-btn {
        margin-bottom: 30px;
      }
    `,
  ],
})
export class AppComponent {
  title = 'bce';
}
