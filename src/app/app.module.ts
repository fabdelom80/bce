import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CastComponent } from './components/cast/cast.component';
import { PersonComponent } from './components/cast/person.component';
import { DetailsSeriesComponent } from './components/series/details-series.component';
import { PreviousEpisodeComponent } from './components/series/previous-episode.component';
import { SeriesCardComponent } from './components/series/series-card.component';
import { BadgeComponent } from './components/shared/badge.component';
import { HomeComponent } from './home.component';
import { NotFoundComponent } from './not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SeriesCardComponent,
    DetailsSeriesComponent,
    BadgeComponent,
    PreviousEpisodeComponent,
    CastComponent,
    PersonComponent,
    NotFoundComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
