import { Component, Input, OnInit } from '@angular/core';
import { Cast } from 'src/shared/models/Show';

@Component({
  selector: 'app-cast',
  template: `
    <img class="img" src="{{ cast.person.image.medium }}" />
    <span>
      <a [routerLink]="['/person', cast.person.id]">{{ cast.person.name }}</a>
      as {{ cast.character.name }}</span
    >
  `,
  styles: [
    `
      .img {
        height: 100%;
      }

      :host {
        display: flex;
        flex-direction: column;
        height: 100%;
      }
    `,
  ],
})
export class CastComponent implements OnInit {
  @Input() cast!: Cast;
  constructor() {}

  ngOnInit(): void {}
}
