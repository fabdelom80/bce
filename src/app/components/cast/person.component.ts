import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Links, Person } from 'src/shared/models/Show';
import { TvMazeService } from 'src/shared/services/tv-maze.service';

@Component({
  selector: 'app-person',
  template: `
    <button (click)="goBack()">Back</button>
    <div class="container" *ngIf="person$ | async as person; else noData">
      <h1>{{ person.name }}</h1>
      <img class="img" src="{{ person.image.medium }}" />
      <span class="country">country : {{ person.country.name }}</span>
      <div>
        <h1>Series :</h1>

        <!-- ngfor doesnt work -->
        <app-series-card
          *ngFor="let series of person._embedded"
          [id]="getId(series._links)"
        ></app-series-card>
      </div>
    </div>

    <ng-template #noData><p>No result</p></ng-template>
  `,
  styles: [
    `
      .container {
        display: flex;
        flex-direction: column;
      }
      .img {
        height: 40vh;
        width: 60vw;
      }
    `,
  ],
})
export class PersonComponent implements OnInit {
  person$: Observable<Person>;
  constructor(
    private activatedRoute: ActivatedRoute,
    private tvMazeService: TvMazeService
  ) {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.person$ = this.tvMazeService.getPersonById(id);
  }

  ngOnInit(): void {}

  getId(links: Links): number {
    return Number(links.show!.href.split('/').pop());
  }

  goBack() {
    window.history.back();
  }
}
