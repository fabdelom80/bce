import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Cast, Show } from 'src/shared/models/Show';
import { TvMazeService } from 'src/shared/services/tv-maze.service';

@Component({
  selector: 'app-details-series',
  template: `
    <button class="home-btn" (click)="resetSearch()" [routerLink]="['']">
      Home
    </button>
    <button (click)="goBack()">Back</button>
    <ng-container *ngIf="show$ | async as show; else noData">
      <div class="card">
        <div class="first-container">
          <img src="{{ show.image.medium }}" />
        </div>
        <div class="second-container">
          <h1 class="title">{{ show.name }}</h1>
          <div class="genre">
            <app-badge
              *ngFor="let genre of show.genres"
              [title]="genre"
            ></app-badge>
          </div>
          <span> <strong> network : </strong> {{ show.network.name }} </span>
          <span><strong>language :</strong> {{ show.language }}</span>
          <span><strong>status :</strong> {{ show.status }}</span>
          <span><strong>premiered :</strong> {{ show.premiered }}</span>
          <span><strong>ended :</strong> {{ show.ended }}</span>
          <span
            ><strong>officialSite :</strong>
            <a href="{{ show.officialSite }}">{{ show.officialSite }}</a></span
          >
          <span><strong>rating :</strong> {{ show.rating.average }}</span>
        </div>
      </div>
      <h1>Summary :</h1>
      <p [innerHTML]="show.summary"></p>

      <h1>Cast</h1>
      <div class="cast">
        <app-cast *ngFor="let item of cast$ | async" [cast]="item"></app-cast>
      </div>

      <app-previous-episode [id]="show.id"></app-previous-episode>
    </ng-container>

    <ng-template #noData><p>No result</p></ng-template>
  `,
  styles: [
    `
      .card {
        display: flex;
        column-gap: 10px;
      }
      .second-container {
        display: flex;
        flex-direction: column;
        row-gap: 5px;
        margin: auto;
      }
      .title {
        text-transform: uppercase;
      }

      .genre {
        display: flex;
        column-gap: 10px;
      }
      app-badge {
        width: 120px;
      }

      .cast {
        height: 20vh;
        display: flex;
        column-gap: 5px;
        flex-direction: column;
        text-align: center;
        flex-wrap: wrap;
      }
    `,
  ],
})
export class DetailsSeriesComponent implements OnInit {
  show$: Observable<Show | undefined>;
  cast$: Observable<Cast[]>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private tvMazeService: TvMazeService
  ) {
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.show$ = this.tvMazeService.getDetailsBySeriesId(id);

    this.cast$ = this.tvMazeService.getCastBySeriesId(id);
  }

  ngOnInit(): void {}

  goBack() {
    window.history.back();
  }

  resetSearch() {
    this.tvMazeService.resetSearch();
  }
}
