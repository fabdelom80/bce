import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { Episode } from 'src/shared/models/Show';
import { TvMazeService } from 'src/shared/services/tv-maze.service';

@Component({
  selector: 'app-previous-episode',
  template: `
    <div *ngFor="let season of seasons; let index = index">
      <h1>Season {{ season }}</h1>
      <div class="container">
        <div class="episod" *ngFor="let item of episode[index]">
          <img class="img" src="{{ item.image.medium }}" />
          {{ item.name }}
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      .container {
        display: flex;
        flex-direction: inherit;
        column-gap: 30px;
      }
      .img {
        height: 5vh;
      }
      .episod {
        width: 100px;
      }
    `,
  ],
})
export class PreviousEpisodeComponent implements OnInit {
  @Input() id!: number;
  episodes$!: Observable<any>;
  seasons: string[] = [];
  episode: Episode[][] = [];
  constructor(private tvMazeService: TvMazeService) {}

  ngOnInit(): void {
    this.tvMazeService
      .getEpisodesBySeriesId(this.id)
      .pipe(first())
      .subscribe((episodes) => {
        const object = episodes.reduce((result, episode) => {
          result[episode.season] = [
            ...(result[episode.season] || ([] as Episode[])),
            episode,
          ];
          return result;
        }, {} as Record<number, Episode[]>);
        this.seasons = Object.keys(object);
        this.episode = Object.values(object);
      });
  }
}
