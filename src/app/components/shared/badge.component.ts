import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-badge',
  template: ` <div class="badge">{{ title }}</div> `,
  styles: [
    `
      .badge {
        border-radius: 50px;
        background-color: #298984;
        text-align: center;
        color: white;
      }
    `,
  ],
})
export class BadgeComponent implements OnInit {
  @Input() title = '';
  constructor() {}

  ngOnInit(): void {}
}
