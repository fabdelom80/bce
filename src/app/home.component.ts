import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Series } from 'src/shared/models/Series';
import { TvMazeService } from 'src/shared/services/tv-maze.service';

@Component({
  selector: 'app-home',
  template: `
    <input
      (change)="search()"
      [(ngModel)]="nameSeries"
      type="text"
      id="search"
      placeholder="Search a series"
    />
    <div class="container" *ngIf="series$ | async as series">
      <app-series-card
        *ngFor="let series of series"
        [show]="series.show"
        [id]="series.show.id!"
      ></app-series-card>
      <p *ngIf="series.length < 1">No result</p>
    </div>
  `,
  styles: [
    `
      .container {
        display: flex;
        flex-wrap: wrap;
        column-gap: 15px;
        row-gap: 15px;
        margin-top: 20px;
      }
    `,
  ],
})
export class HomeComponent implements OnInit {
  nameSeries = '';
  series$: Observable<Series[]> = of();

  constructor(private tvMazeService: TvMazeService) {
    this.series$ = this.tvMazeService.getSeries();
    this.nameSeries = this.tvMazeService.gettNameSearch();
  }

  ngOnInit(): void {}

  search() {
    this.tvMazeService.setNameSearch(this.nameSeries);
    this.tvMazeService.search();
  }
}
