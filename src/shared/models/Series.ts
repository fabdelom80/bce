import { Show } from './Show';

export type Series = {
  score: number;
  show: Show;
};
