export type Days =
  | 'Monday'
  | 'Tuesday'
  | 'Wednesday'
  | 'Thursday'
  | 'Friday'
  | 'Saturday'
  | 'Sunday';

export type Schedule = {
  time: string;
  days: Days[];
};

export type Rating = {
  average: number;
};

export type Country = {
  name: string;
  code: string;
  timezone: string;
};
export type Network = {
  id: number;
  name: string;
  country: Country;
  officialSite: string;
};

export type Externals = {
  tvrage: number;
  thetvdb: number;
  imdb: string;
};

export type Image = {
  medium: string;
  original: string;
};

export type OtherUrl = {
  href: string;
};

export type Links = {
  self?: OtherUrl;
  previousepisode?: OtherUrl;
  show?: OtherUrl;
  character?: OtherUrl;
};

export interface Movie extends Base {
  type: string;
  runtime: number;
  rating: Rating;
  summary: string;
}

export interface Show extends Movie {
  language: string;
  genres: string[];
  status: string;
  averageRuntime: number;
  premiered: Date;
  ended: Date;
  officialSite: string;
  schedule: Schedule;
  weight: number;
  network: Network;
  webChannel?: null;
  dvdCountry?: null;
  externals: Externals;
  updated: number;
}

export interface Episode extends Movie {
  season: number;
  number: number;
  airdate: Date;
  airtime: string;
  airstamp: Date;
}

export interface Base {
  id: number;
  url: string;
  name: string;
  image: Image;
  _links: Links;
}

export interface Person extends Base {
  country: Country;
  birthday: Date;
  deathday?: Date;
  gender: String;
  updated: number;
  _embedded?: CastCredits[];
}

export type CastCredits = {
  self: boolean;
  voice: boolean;
  _links: Links;
};

export type Cast = {
  person: Person;
  character: Base;
  self: boolean;
  voice: boolean;
};
