import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, first, Observable } from 'rxjs';
import { Series } from '../models/Series';
import { Cast, Episode, Person, Show } from '../models/Show';

@Injectable({
  providedIn: 'root',
})
export class TvMazeService {
  private url: string = 'https://api.tvmaze.com';
  private series = new BehaviorSubject<Series[]>([]);
  private nameSearch = '';
  constructor(private httpClient: HttpClient) {}

  public search() {
    this.httpClient
      .get<Series[]>(`${this.url}/search/shows?q=${this.nameSearch}`)
      .pipe(first())
      .subscribe((series) => this.series.next(series));
  }

  public setNameSearch(nameSearch: string) {
    this.nameSearch = nameSearch;
  }

  public gettNameSearch(): string {
    return this.nameSearch;
  }

  public resetSearch() {
    this.nameSearch = '';
    this.series = new BehaviorSubject<Series[]>([]);
  }

  public getSeries(): Observable<Series[]> {
    return this.series.asObservable();
  }

  public getDetailsBySeriesId(idSeries: number): Observable<Show> {
    return this.httpClient.get<Show>(`${this.url}/shows/${idSeries}`);
  }

  public getEpisodesBySeriesId(idSeries: Number): Observable<Episode[]> {
    return this.httpClient.get<Episode[]>(
      `${this.url}/shows/${idSeries}/episodes`
    );
  }

  public getCastBySeriesId(idSeries: Number): Observable<Cast[]> {
    return this.httpClient.get<Cast[]>(`${this.url}/shows/${idSeries}/cast`);
  }

  public getPersonById(idPerson: Number): Observable<Person> {
    return this.httpClient.get<Person>(
      `${this.url}/people/${idPerson}?embed=castcredits`
    );
  }
}
